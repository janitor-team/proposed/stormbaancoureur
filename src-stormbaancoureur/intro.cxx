/*
 * stormbaancoureur
 * (c) 2006,2007,2008 by Bram Stolk
 * bram at gmail.com
 * LICENSED ACCORDING TO THE GPLV3
 */

#include <assert.h>
#include <stdio.h>
#include <GL/gl.h>
#include <plib/ul.h>

#include <plib/ssg.h>

#include "soundenginealsa.h"
#include "intro.h"



Engine::Engine(ssgEntity *pistonmodel, ssgEntity *rodmodel)
{
  float a = M_PI/4.0f;
  static float angles[8] = {-a,a,-a,a,-a,a,-a,a};
  static float x[8] = {-2.5, -2.4, -0.9, -0.8, 0.7, 0.8, 2.3, 2.4};
  root = new ssgRoot();
  for (int i=0; i<8; i++)
  {
    sgVec3 axis={1,0,0};
    sgMat4 m;
    sgMakeRotMat4(m, 180/M_PI*angles[i], axis);
    sgSetVec3(m[3], x[i],0,0);
    cyl_trfs[i] = new ssgTransform();
    cyl_trfs[i]->setTransform(m);
    piston_trfs[i] = new ssgTransform();
    piston_trfs[i]->addKid(pistonmodel);
    rod_trfs[i] = new ssgTransform();
    rod_trfs[i]->addKid(rodmodel);
    cyl_trfs[i]->addKid(piston_trfs[i]);
    cyl_trfs[i]->addKid(rod_trfs[i]);
    root->addKid(cyl_trfs[i]);
  }
#if 1
  EnableBlending(pistonmodel);
  EnableBlending(rodmodel);
#endif
}


Engine::~Engine()
{
  for (int i=0; i<8; i++)
  {
    delete cyl_trfs[i];
    delete rod_trfs[i];
    delete piston_trfs[i];
  }
  delete root;
}


void Engine::EnableBlending(ssgEntity *e)
{
  ssgBranch *b = dynamic_cast<ssgBranch*>(e);
  if (b)
  { 
    for (int i=0; i<b->getNumKids(); i++)
      EnableBlending(b->getKid(i));
  }
  ssgLeaf *l = dynamic_cast<ssgLeaf*>(e);
  if (l)
  {
    if (l->hasState())
    {
      ssgSimpleState *s = dynamic_cast<ssgSimpleState*> (l->getState());
      s->enable(GL_BLEND);
    }
  }
}


void Engine::UpdateCylinder(int nr, float angle)
{
  const float excentric = 0.2f;
  const float rodlen = 1.30f;
  float yo = cosf(-angle) * excentric;
  float zo = sinf(-angle) * excentric;
  float rodangle = 0.5*M_PI - acosf(yo / rodlen);
  sgVec3 axis={1,0,0};
  sgMat4 m;
  sgMakeRotMat4(m, 180/M_PI*rodangle, axis);
  sgSetVec3(m[3], 0.0, yo, zo);
  rod_trfs[nr]->setTransform(m);
  sgMakeTransMat4(m, 0.0, 0.0, rodlen*cosf(rodangle) + zo);
  piston_trfs[nr]->setTransform(m);
}


void Engine::UpdateCylinders(float angle)
{
  // Firing order of small block chevy is cyl 1-8-4-3-6-5-7-2

  float angles[8];
  const float hp = M_PI/2;
  int i;
  for (i=0; i<8; i++)
    angles[i] = angle;

  // bank at driver side is 90deg earlier than bank at passenger side
  angles[1] += hp;
  angles[3] += hp;
  angles[5] += hp;
  angles[7] += hp;

  // crank pivot at 90deg
  angles[4] += hp;
  angles[5] += hp;

  // crank pivot at 180deg
  angles[6] += 2*hp;
  angles[7] += 2*hp;

  // crank pivot at 270deg
  angles[2] += 3*hp;
  angles[3] += 3*hp;

  for (i=0; i<8; i++)
    UpdateCylinder(i, angles[i]);
}


void Engine::Redraw(void)
{
  ssgCullAndDraw(root);
}


void Intro::Redraw(int winw, int winh)
{
  ssgGetLight(0)->off();
  ssgGetLight(1)->on();

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glFrustum(-1,1,-1,1,1,100);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  glTranslated(0, 0, 0);

  glBlendFunc(GL_ONE,GL_ONE);
  //glColor3f(1,1,1);

  for (int i=0; i<SNAPS; i++)
  {
    curangle = fmodf(curangle + angularstep, 4*M_PI);
    engine->UpdateCylinders(curangle);
    engine->Redraw();
    glClear(GL_DEPTH_BUFFER_BIT);
  }

  glBlendFunc(GL_ONE,GL_ZERO);
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  ssgGetLight(1)->off();
  ssgGetLight(0)->on();
}


Intro::Intro(SoundEngineAlsa *se, const std::string &pref, ssgEntity *pistonmodel, ssgEntity *rodmodel) :
  prefix(pref),
  soundengine(se),
  simtime(0.0),
  curangle(0.0)
{
  std::string fname = pref + "/sounds/rpm_graph.txt";
  FILE *f = fopen(fname.c_str(),"r");
  if (!f)
    fprintf(stderr,"Cannot load '%s'\n", fname.c_str());
  assert(f);
  int rv=fscanf(f,"graphsize %d time %f ", &graphsz, &graphtime);
  assert(rv==2);

  revs = new int[graphsz];
  for (int i=0; i<graphsz; i++)
    assert(fscanf(f,"%d ", revs+i) == 1);
  fclose(f);

  fname = pref + "/sounds/camaro_s16_le.wav";
  se->Play(fname);

  engine = new Engine(pistonmodel, rodmodel);

  ssgLight *light=ssgGetLight(1);
  const float ka = 0.3f;
  const float kd = 1.0f;
  const float ks = 1.0f;
  light->setColour(GL_DIFFUSE,  kd,kd,kd);
  light->setColour(GL_AMBIENT,  ka,ka,ka);
  light->setColour(GL_SPECULAR, ks,ks,ks);
  light->setPosition(0,0,8);
  light->setSpotlight(true);
  light->setSpotDirection(0,0,-1);
  light->setSpotDiffusion(100,180);
}


Intro::~Intro()
{
  delete [] revs;
}


bool Intro::Sustain(float dt, float fractiondone)
{
  // Should not happen
  if (fractiondone>1.0)
    fractiondone=1.0;
  // Should not happen
  if (fractiondone<0.0)
  {
    fprintf(stderr,"intro.cxx: fractiondone=%f\n", fractiondone);
    fractiondone=0.0;
  }
  int idx0 = int (fractiondone * (graphsz-1));
  if (idx0 >= graphsz-1) return true;
  int idx1 = idx0+1;
  float step = 1.0 / (graphsz-1);
  float f = (fractiondone - idx0*step)/step;
  assert(f>=0.0);
  assert(f<=1.0);
  int r0 = revs[idx0];
  int r1 = revs[idx1];
  rev = (int) (f * r1 + (1.0-f) * r0);
  rev = rev/60; // rpm to rps
  rev = rev/2; // slowmotion
  angularstep = dt * M_PI * rev / SNAPS;

  return false;
}

