#ifndef PLODEGUI_H
#define PLODEGUI_H

#include <stdlib.h>	// for exit()

#include "vectortext.h"
#include "postscore.h"

#define GUIH 11

class PlodeGUI
{
  public:

    PlodeGUI(const char *uname, bool gamepad) :
      username(uname),
      hasgamepad(gamepad),
      hasmenu(true),
      showingleaders(false),
      showinghelp(false),
      leaderboardtext(0),
      choice(3),
      done(false),
      leaderboardposition(-1)
    {
      if (gamepad)
        lines[0] = "EXAMINE GAMEPAD CONTROLS";
      else
        lines[0] = "EXAMINE DEFAULT KEY CONTROLS";
      lines[1] = "EXAMINE LEADERBOARD";
      lines[2] = "ACCEPT DEFEAT";
      lines[3] = "PRACTICE";
      lines[4] = "ATTACK STORMBAAN";
      lines[5] = "TEST GROUND";
    }


    ~PlodeGUI()
    {
    }


    void SetLeaderBoardText(const char *text)
    {
      leaderboardtext = text;
      if (!text)
      {
        postscore_put("---", 0.0);
        leaderboardtext = postscore_get();
      }
      if (leaderboardtext)
      {
        // parse the leaderboard
        leadernames.clear();
        leadertimes.clear();
        int numscores = strlen(leaderboardtext) / 16;
        for (int i=0; i<numscores; i++)
        {
          char  name[16];
          float tim;
          int rv=sscanf(leaderboardtext+16*i, "%s %f", name, &tim);
          assert(rv==2);
          leadernames.push_back(name);
          leadertimes.push_back(tim);
        }
      }
      showingleaders = true;
      choice=1;
      hasmenu = true;
    }


    const char *LeaderBoardLine(int idx)
    {
      static char line[80];
      assert(idx < (int) leadernames.size());
      sprintf(line,"%02d   %-8s   %6.2f", idx+1, leadernames[idx].c_str(), leadertimes[idx]);
      for (unsigned int i=0; i<strlen(line); i++)
        line[i] = toupper(line[i]);
      return line;
    }


    void ShowHelp(void)
    {
      std::string lines[6];
      if (hasgamepad)
      {
        lines[0] = "ACCEL    R SHOULDER";
        lines[1] = "BRAKE    L SHOULDER";
        lines[2] = "STEER L  FIRST STICK";
        lines[3] = "STEER R  FIRST STICK";
        lines[4] = "ACTION   FIRST BUTTON";
        lines[5] = "EBRAKE   SECOND BUTTON";
      }
      else
      {
        lines[0] = "ACCEL    KEY A";
        lines[1] = "BRAKE    KEY Z OR KEY Y";
        lines[2] = "STEER L  KEY COMMA";
        lines[3] = "STEER R  KEY DOT";
        lines[4] = "ACTION   KEY SPACEBAR";
        lines[5] = "EBRAKE   KEY B OR KEY TAB";
      }

      int i;
      for (i=0; i<6; i++)
      {
        vt_set_pos(10, 9.5-i*1.2);
        vt_print(lines[i], false);
      }
    }


    void ShowLeaders(void)
    {
      if (!leaderboardtext)
      {
        vt_set_pos(14,5.0);
        vt_print("LEADERBOARD IS", true);
        vt_set_pos(14,3.5);
        vt_print("NOT AVAILABLE", true);
        return;
      }
      // Determine which part of leaderboard to show!
      unsigned int i;
      leaderboardposition=-1;
      for (i=0; i<leadernames.size() && leaderboardposition==-1; i++)
        if (leadernames[i] == username)
          leaderboardposition=i;
      if (leaderboardposition != -1)
        lines[2] = "RETIRE IN GLORY";
      int offset = leaderboardposition-5;
      if (offset<0) offset=0;
      while (offset+GUIH > (int) leadernames.size()) offset--;
      for (int i=0; i<GUIH; i++)
      {
        vt_set_pos(4, 12.6 - 1.2*i);
        const char *line = LeaderBoardLine(i+offset);
        vt_print(line, false);
      }
    }


    void Redraw(int winw, int winh, char *fps)
    {
      static int framecnt=0;
      if (framecnt<45) framecnt++;
      vt_init(framecnt, framecnt/3.0);

      vt_set_colour(1,1.0,1,0.7);
      vt_set_pos(44-strlen(fps), 13.5);
      vt_print(fps, false);

      if (showingleaders)
        ShowLeaders();

      if (showinghelp)
        ShowHelp();

      if (hasmenu && !showingleaders && !showinghelp)
      {
        for (int i=0; i<6; i++)
        {
          if (choice==i)
            vt_set_colour(1.0,1.0,0.2,0.8);
          else
            vt_set_colour(0.7,0.7,0.7,0.5);
          vt_set_pos(10, 9.5 - 1.4*i);
          vt_print(lines[i], false);
        }
      }

      vt_exit();
    }

    void ChangeChoice(int delta)
    {
      choice += delta;
      if (choice>5) choice=0;
      if (choice<0) choice=5;
    }

    const std::string &SelectChoice(void)
    {
      if (showingleaders)
      {
        showingleaders = false;
        return lines[1];
      }
      if (showinghelp)
      {
        showinghelp = false;
        return lines[0];
      }
      if (choice==0)
      {
        showinghelp = true;
      }
      if (choice==1)
      {
        showingleaders = true;
        if (!leaderboardtext) SetLeaderBoardText(0);
      }
      if (choice==2)
      {
        exit(0);
      }
      if (choice==3)
      {
        hasmenu=false;
      }
      if (choice==4)
      {
        hasmenu=false;
      }
      if (choice==5)
      {
        hasmenu=false;
      }
      return lines[choice];
    }

    std::string username;
    bool hasgamepad;
    bool hasmenu;
    bool showingleaders;
    bool showinghelp;
    const char *leaderboardtext;

  protected:
    int choice;
    bool done;
    std::string lines[GUIH];
    std::vector<std::string> leadernames;
    std::vector<float>       leadertimes;
    int leaderboardposition;
};

#endif

