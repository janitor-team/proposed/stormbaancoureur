#ifndef INTRO_H
#define INTRO_H

#include <string>

class SoundEngineAlsa;

class ssgTransform;
class ssgEntity;
class ssgRoot;

class Engine
{
  public:
    Engine(ssgEntity *pistonmodel, ssgEntity *rodmodel);
    ~Engine();
    void UpdateCylinder(int nr, float a);
    void UpdateCylinders(float a);
    void Redraw(void);
    void EnableBlending(ssgEntity *e);
  protected:
    ssgTransform *cyl_trfs[8];
    ssgTransform *piston_trfs[8];
    ssgTransform *rod_trfs[8];
    ssgRoot *root;
};


class Intro
{
  public:
    Intro(SoundEngineAlsa *se, const std::string &pref, ssgEntity *pistonmodel, ssgEntity *rodmodel);
    ~Intro();
    void Redraw(int winw, int winh);
    bool Sustain(float fractiondone, float dt);
  protected:
    const std::string &prefix;
    SoundEngineAlsa *soundengine;
    int graphsz;
    float graphtime;
    float simtime;
    int rev;
    int *revs;
    float curangle;
    float angularstep;
    Engine *engine;
    static const int SNAPS=8;
};

#endif

